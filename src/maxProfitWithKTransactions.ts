export function maxProfitWithKTransactions(prices: number[], k: number): number {
   if (prices.length < 2 || k === 0) {
      return 0;
   }
   const profits: number[][] = new Array(k + 1).fill(0).map(() => new Array(prices.length).fill(0));
   // d = day, t = transaction
   for (let t = 1; t < k + 1; t++) {
      let maxThusFar = -Infinity;
      for (let d = 1; d < prices.length; d++) {
         maxThusFar = Math.max(maxThusFar, profits[t - 1][d - 1] - prices[d - 1]);
         profits[t][d] = Math.max(profits[t][d - 1], maxThusFar + prices[d]);
      }
   }
   return profits[k][prices.length - 1];
}